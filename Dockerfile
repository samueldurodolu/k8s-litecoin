FROM debian:jessie

ARG LITECOIN_VERSION="0.18.1"
ARG LITECOIN_VERSION_SHA256="ca50936299e2c5a66b954c266dcaaeef9e91b2f5307069b9894048acf3eb5751"

ENV RPCUSER=""
ENV RPCPASSWPORD=""

RUN apt-get update \
    && apt-get install -y \
        curl \
        sudo \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && useradd -m litecoin \
    && usermod -aG sudo litecoin \
    && echo "%sudo ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers \
    && chmod 0701 /home/litecoin \
    && mkdir /home/litecoin/.litecoin

USER litecoin
WORKDIR /home/litecoin

RUN curl -Ls -o litecoin-${LITECOIN_VERSION}-x86_64-linux-gnu.tar.gz https://download.litecoin.org/litecoin-${LITECOIN_VERSION}/linux/litecoin-${LITECOIN_VERSION}-x86_64-linux-gnu.tar.gz && \
echo "${LITECOIN_VERSION_SHA256} litecoin-${LITECOIN_VERSION}-x86_64-linux-gnu.tar.gz" | sha256sum -c -

RUN tar xvzf litecoin-${LITECOIN_VERSION}-x86_64-linux-gnu.tar.gz \
  && sudo chmod +x ./litecoin-${LITECOIN_VERSION}/bin/* && sudo cp ./litecoin-${LITECOIN_VERSION}/bin/* /usr/local/bin/

EXPOSE 9332 9333

ENTRYPOINT ["litecoind", "-printtoconsole"]